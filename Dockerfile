FROM scratch

LABEL maintaner="Chris Pauley"

COPY . .

EXPOSE 8080

CMD ["./main"]
